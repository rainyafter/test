/* main - map 맵 가게 리스트 감추기, 보이기 */
var map_toggle = $('.button-map-toggle');
var map_toggle2 = $('.button-map-toggle-v2');
var map_view = $('.map-shop-list');

$(document).ready( function() {
    $(map_toggle).click( function() {
        if($(this).hasClass("off")) {
            $(this).addClass("off").removeClass("on");
        } else {
            $(this).addClass("on").removeClass("off");
        }
        $(this).toggleClass("on off");
        $(map_view).toggle( "slide", function() {
            // Animation complete.
        });
    });
    $(map_toggle2).click( function() {
        $(map_view).toggle( "slide", function() {
        });
        $(map_toggle).addClass("off").removeClass("on");
    });
});

/* main - search 공통 - 자동검색 지우기, 지우면 모달 같이 변경됨 수정바람 */
$( ".common-search-all-delete").click(function() {
    $( ".common-search-list").empty();
  });
  $( ".common-search-list .common-search-delete").click(function() {
    $(this).parent('li').remove();
  });


/* main - search 모달 input active 색상 변경 */
$( ".search-box-focus").keypress(function() {
    $(".modal-location-search").addClass('focus');
});
$( ".search-box-focus").mouseleave(function() {
    $(".modal-location-search").removeClass('focus');
});

/* main - 리스트 클릭시 가맹점 맵 활성화 */
var map_list = $(".map-list-box .common-list-list");
var map_list_close = $(".common-list-box-view .close-btn");
$( ".common-list-box-view").hide();

$(map_list).click(function() {
    $(".common-list-box-view").show();
    $(this).addClass('active');
});
$(map_list_close).click(function() {
    $(".common-list-box-view").hide();
    $(map_list).removeClass('active');
});



/* 가입 - 동의 */
// 체크박스 개별 선택
$(function(){ 
    $("#check_all").click(function(){ 
        if($("#check_all").prop("checked")) { 
             $("input[type=checkbox]").prop("checked",true); 
       } else {
             $("input[type=checkbox]").prop("checked",false); } })
 });


 /* 갤러리 박스 */
 Fancybox.bind('[data-fancybox="gallery"]', {
    //dragToClose: false,
    Thumbs: false,
  
    Image: {
      zoom: false,
      click: false,
      wheel: "slide",
    },
  
    on: {
      // Move caption inside the slide
      reveal: (f, slide) => {
        slide.$caption && slide.$content.appendChild(slide.$caption);
      },
    },
  });